import React, { Component } from 'react';

import {
  Appear, Deck, Fill,
  Heading, Layout, ListItem, List, Link, Slide, Spectacle,
  Text, CodePane
} from "spectacle";

import Demo from './Demo';

import ConsoleLogButton from './demo/ConsoleLogButton';
import ConsoleTableButton from './demo/ConsoleTableButton';
import ConsoleTraceButton from './demo/ConsoleTraceButton';
import DebuggerButton from './demo/DebuggerButton';
import ChromeDevToolsElements from './demo/ChromeDevToolsElements';
import ChromeDevToolsSource from './demo/ChromeDevToolsSource';
import ChromeDevToolsApplication from './demo/ChromeDevToolsApplication';

import 'normalize.css';
import react from "./assets/react.svg";

import createTheme from "spectacle/lib/themes/default";

const images = {
  react
};

const theme = createTheme({
  primary: "#6196cc"
});

export default class Presentation extends Component {
  render() {
    return (
      <Spectacle theme={theme}>
        <Deck transition={["zoom", "slide"]} transitionDuration={500}>
          <Slide transition={["zoom"]} bgColor="primary">
            <Heading size={1} fit caps lineHeight={1} textColor="black">
              REACT + REDUX
            </Heading>
            <Heading size={2} fit caps textColor="white">
              Debugging y DevTools
            </Heading>
          </Slide>
          <Slide transition={["slide"]} bgColor="black" notes="You can even put notes on your slide. How awesome is that?">
            <Heading size={4} caps textAlign="left" textColor="primary" textFont="primary">
              Hoy hablaremos de:
            </Heading>
            <List>
              <Appear><ListItem textColor="white">Debugging</ListItem></Appear>
              <Appear><ListItem textColor="white">React DevTools</ListItem></Appear>
              <Appear><ListItem textColor="white">Redux DevTools</ListItem></Appear>
            </List>
          </Slide>
          <Slide transition={["slide"]}  bgImage={images.react.replace("/", "")} bgColor="black" bgDarken={0.75}>
            <Heading size={1} caps textColor="primary">
              Debugging
            </Heading>
          </Slide>
          <Slide transition={["slide"]} bgColor="primary">
            <Heading size={2} textColor="tertiary">console.log()</Heading>
            <Text textColor="secondary">Imprime en pantalla el mensaje pasado por parametro</Text>
            <Layout>
              <Fill>
                <CodePane
                  lang="jsx"
                  source={require("raw!./exampleCode/ConsoleLogButton.example")}
                  margin="20px auto"
                />
              </Fill>
              <Fill>
                <Demo>
                  <ConsoleLogButton/>
                </Demo>
              </Fill>
            </Layout>
          </Slide>
          <Slide transition={["slide"]} bgColor="primary">
            <Heading size={2} textColor="tertiary">console.table()</Heading>
            <Text textColor="secondary">Imprime una tabla en base al objeto pasado por parametro</Text>
            <Layout>
              <Fill>
                <CodePane
                  lang="jsx"
                  source={require("raw!./exampleCode/ConsoleTableButton.example")}
                  margin="20px auto"
                />
              </Fill>
              <Fill>
                <Demo>
                  <ConsoleTableButton/>
                </Demo>
              </Fill>
            </Layout>
          </Slide>
          <Slide transition={["slide"]} bgColor="primary">
            <Heading size={2} textColor="tertiary">console.trace()</Heading>
            <Text textColor="secondary">Imprime los puntos de la aplicación por las que ha pasado hasta llegar ese punto</Text>
            <Layout>
              <Fill>
                <CodePane
                  lang="jsx"
                  source={require("raw!./exampleCode/ConsoleTraceButton.example")}
                  margin="20px auto"
                />
              </Fill>
              <Fill>
                <Demo>
                  <ConsoleTraceButton/>
                </Demo>
              </Fill>
            </Layout>
          </Slide>
          <Slide transition={["slide"]} bgColor="primary">
            <Heading size={2} textColor="tertiary">debugger;</Heading>
            <Text textColor="secondary">Se introduce en el código y para la ejecución en el punto indicado</Text>
            <Layout>
              <Fill>
                <CodePane
                  lang="jsx"
                  source={require("raw!./exampleCode/DebuggerButton.example")}
                  margin="20px auto"
                />
              </Fill>
              <Fill>
                <Demo>
                  <DebuggerButton/>
                </Demo>
              </Fill>
            </Layout>
          </Slide>
          <Slide transition={["slide"]}  bgImage={images.react.replace("/", "")} bgColor="black" bgDarken={0.75} notes="Explicar porque es importante, beneficios de productividad y economicos">
            <Heading size={1} caps textColor="primary">
              Google DevTools
            </Heading>
          </Slide>
          <Slide transition={["slide"]} bgColor="primary" notes="Inspeccionar con la flecha, click izquierdo inspeccionar, Modificar colores, modificar DOM, mover posicion, ">
            <Heading size={2} textColor="tertiary">Pestaña Elements</Heading>
            <Text textColor="secondary" margin="20px 0">Se introduce en el código y para la ejecución en el punto indicado</Text>
            <Layout>
              <Fill>
                <Text textAlign="left">Permite:</Text>
                <List>
                  <ListItem textColor="white">Inspeccionar la pagina</ListItem>
                  <ListItem textColor="white">Editar estilos</ListItem>
                  <ListItem textColor="white">Editar el DOM</ListItem>
                </List>
              </Fill>
              <Fill>
                <Demo>
                  <ChromeDevToolsElements/>
                </Demo>
              </Fill>
            </Layout>
          </Slide>
          <Slide transition={["slide"]} bgColor="primary" notes="Lanzar diferentes operaciones o comandos, filtrar por text, error o warning, clear console, preserve log, guardar">
            <Heading size={2} textColor="tertiary">Pestaña Console</Heading>
            <Text textColor="secondary" margin="20px 0">Muestra información a los desarrolladores y permite ejecutar JS</Text>
          </Slide>
          <Slide transition={["slide"]} bgColor="primary" notes="Introducir breakpoints (condicionales, deshabilitar y eliminar desde lateral), revisar callstack y scope, uncaught exceptions, desofuscar el bundle.js">
            <Heading size={2} textColor="tertiary">Pestaña Sources</Heading>
            <Text textAlign="left" margin="20px 0 0">Permite:</Text>
            <List>
              <ListItem textColor="white">Introducir puntos de parada</ListItem>
              <ListItem textColor="white">Debugging en base a eventos</ListItem>
              <ListItem textColor="white">Desofuscar codigo minificado</ListItem>
            </List>
          </Slide>
          <Slide transition={["slide"]} bgColor="primary" notes="">
            <Heading size={2} textColor="tertiary">Pestaña Sources</Heading>
            <Layout>
              <Fill>
                <CodePane
                  lang="jsx"
                  source={require("raw!./exampleCode/ChromeDevToolsSource.example")}
                  margin="20px auto"
                />
              </Fill>
              <Fill>
                <Demo>
                  <ChromeDevToolsSource/>
                </Demo>
              </Fill>
            </Layout>
          </Slide>
          <Slide transition={["slide"]} bgColor="primary" notes="">
            <Heading size={2} textColor="tertiary">Pestaña Network</Heading>
            <Text textAlign="left" margin="20px 0 0">Permite:</Text>
            <List>
              <ListItem textColor="white">Listar y filtrar las cargas de que realiza la web</ListItem>
              <ListItem textColor="white">Capturar imagenes durante la carga</ListItem>
              <ListItem textColor="white">Simular conexiones lentas</ListItem>
            </List>
          </Slide>
          <Slide transition={["slide"]} bgColor="primary" notes="">
            <Heading size={2} textColor="tertiary">Pestañas Timeline y Profiles</Heading>
            <Text textAlign="left" margin="20px 0 0">Permite:</Text>
            <List>
              <ListItem textColor="white">Analizar rendimiento de la aplicación</ListItem>
            </List>
          </Slide>
          <Slide transition={["slide"]} bgColor="primary" notes="">
            <Heading size={2} textColor="tertiary">Pestaña Application</Heading>
            <Text textAlign="left" margin="20px 0 0">Permite:</Text>
            <List>
              <ListItem textColor="white">Analizar los datos que gestiona nuestra aplicación</ListItem>
            </List>
          </Slide>
          <Slide transition={["slide"]} bgColor="primary" align="center top" notes="">
            <Heading size={2} textColor="tertiary">Pestaña Application</Heading>
            <Layout>
              <Fill>
                <CodePane
                  lang="jsx"
                  source={require("raw!./exampleCode/ChromeDevToolsApplication.example")}
                  margin="20px auto"
                />
              </Fill>
              <Fill>
                <Demo>
                  <ChromeDevToolsApplication/>
                </Demo>
              </Fill>
            </Layout>
          </Slide>
          <Slide transition={["slide"]}  bgImage={images.react.replace("/", "")} bgColor="black" bgDarken={0.75}>
            <Heading size={1} caps textColor="primary">
              React DevTools
            </Heading>
          </Slide>
          <Slide transition={["slide"]} bgColor="primary" notes="">
            <Heading size={2} textColor="tertiary">Instalación</Heading>
            <Text margin="20px 0 0">Descargable como plugin para
              <Link href="https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi" target="_blank"> Chrome</Link> y
              <Link href="https://addons.mozilla.org/firefox/addon/react-devtools/" target="_blank"> Firefox</Link>.
            </Text>
          </Slide>
          <Slide transition={["slide"]} bgColor="primary" notes="">
            <Text textAlign="left" margin="20px 0 0">Permite:</Text>
            <List>
              <ListItem textColor="white">Manipular los estados y propiedades de nuestros componentes</ListItem>
              <ListItem textColor="white">Filtrado y búsqueda de componentes</ListItem>
              <ListItem textColor="white">Guardar en variables temporales funciones y propiedades del componente</ListItem>
            </List>
          </Slide>
          <Slide transition={["slide"]} bgColor="primary" notes="Editar props y estado, $r, right click $tmp, flecha roja - contexto / estado, Mostrar AirBnb">
            <Heading size={2} textColor="tertiary">Manipulando nuestro componente</Heading>
            <Layout>
              <Fill>
                <Demo>
                  <ChromeDevToolsApplication/>
                </Demo>
              </Fill>
            </Layout>
          </Slide>
          <Slide transition={["slide"]}  bgImage={images.react.replace("/", "")} bgColor="black" bgDarken={0.75}>
            <Heading size={1} caps textColor="primary">
              Redux DevTools
            </Heading>
          </Slide>
          <Slide transition={["slide"]} bgColor="primary" notes="">
            <Heading size={2} textColor="tertiary">Instalación</Heading>
            <Text margin="20px 0 0">Descargable como plugin para
              <Link href="https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd" target="_blank"> Chrome</Link> y
              <Link href="https://addons.mozilla.org/en-US/firefox/addon/remotedev/" target="_blank"> Firefox</Link>.
            </Text>
            <Text margin="20px 0 0">Requiere
              <Link href="https://github.com/zalmoxisus/redux-devtools-extension#1-with-redux" target="_blank"> configurar un middleware</Link> en el store
            </Text>
          </Slide>
          <Slide transition={["slide"]} bgColor="primary" notes="">
            <Text textAlign="left" margin="20px 0 0">Permite:</Text>
            <List>
              <ListItem textColor="white">Revisar todos los cambios de estado y los datos</ListItem>
              <ListItem textColor="white">Revertir los cambios para volver al estado inicial</ListItem>
              <ListItem textColor="white">Interfaz para interactuar y movernos a través de los estados</ListItem>
            </List>
          </Slide>
          <Slide transition={["slide"]} bgColor="primary" notes="">
            <Heading size={2} textColor="tertiary">Demos</Heading>
            <Text margin="20px 0 0">
              <Link href="http://zalmoxisus.github.io/examples/counter/" target="_blank">Demo 1</Link><br/>
              <Link href="http://zalmoxisus.github.io/examples/todomvc/" target="_blank"> Demo 2</Link>
            </Text>
          </Slide>
        </Deck>
      </Spectacle>
    );
  }
}
