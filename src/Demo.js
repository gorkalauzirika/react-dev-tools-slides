import React from 'react';

const Demo = (props) => {
  return (
    <div style={{display: 'flex', alignItems: 'center', height: '100%', justifyContent: 'center'}}>
      {props.children}
    </div>
  )
};

export default Demo;
