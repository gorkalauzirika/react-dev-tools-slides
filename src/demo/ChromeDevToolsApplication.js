import React from 'react';

import Button from './Button';

class ChromeDevToolsApplication extends React.Component {
  constructor() {
    super();
    this.state = {
      counter: 0
    };
  }

  increase() {
    this.setState({counter: this.state.counter + 1})
  }

  decrease() {
    this.setState({counter: this.state.counter - 1})
  }

  save() {
    localStorage.setItem('counter',this.state.counter);
  }

  load() {
    this.setState({
      counter: parseInt(localStorage.getItem('counter'), 10)
    });
  }

  render() {
    return (
      <div>
        <span>Contador: {this.state.counter}</span><br/>
        <Button color="white" onClick={this.increase.bind(this)}>Incrementar</Button><br/>
        <Button color="white" onClick={this.decrease.bind(this)}>Decrementar</Button><br/>
        <Button color="white" onClick={this.save.bind(this)}>Guardar en localstorage</Button><br/>
        <Button color="white" onClick={this.load.bind(this)}>Obtener de localstorage</Button>
      </div>
    )
  }
}

export default ChromeDevToolsApplication;

