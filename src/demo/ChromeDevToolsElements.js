import React from 'react';

const ChromeDevToolsElements = () => (
  <div>
    <p style={{background:'red'}}>Esto es un párrafo rojo</p>
    <p style={{background:'green'}}>Esto es un párrafo verde</p>
    <p style={{background:'yellow'}}>Esto es un párrafo amarillo</p>
  </div>
);

export default ChromeDevToolsElements;

