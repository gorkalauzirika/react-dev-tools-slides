import React from 'react';

class ConsoleTableButton extends React.Component {
  onClick() {
    console.table([{
      curso: 'React + Redux',
      duracion: 20
    }, {
      curso: 'Symfony',
      duracion: 30
    }]);
  }

  render() {
    return <button onClick={this.onClick}>Table!!</button>
  }
}

export default ConsoleTableButton;

