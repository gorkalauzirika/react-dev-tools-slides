import React from 'react';

class Button extends React.Component {
  render() {
    var { color, children, ...props } = this.props;
    const background = color || 'white';
    return (
      <button style={{background}} {...props}>
        {children}
      </button>
    );
  }
}

Button.propTypes = {
  color: React.PropTypes.string
};

export default Button;
