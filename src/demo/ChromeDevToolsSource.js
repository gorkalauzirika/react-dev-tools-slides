import React from 'react';

class ChromeDevToolsSource extends React.Component {
  stopAlways() {
    console.log('Stop always');
  }

  stopConditional() {
    const value = Math.floor(Math.random()*2);
    console.log('Stop conditional value: ' + value);
  }

  stopDisabled() {
    console.log('Stop disabled');
  }

  render() {
    return (
      <div>
        <button onClick={this.stopAlways}>Parar siempre</button>
        <button onClick={this.stopConditional}>Parada condicional</button>
        <button onClick={this.stopDisabled}>Parada deshabilitada</button>
      </div>
    )
  }
}

export default ChromeDevToolsSource;

