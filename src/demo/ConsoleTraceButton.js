import React from 'react';

class ConsoleTraceButton extends React.Component {
  onClick() {
    console.trace();
  }

  render() {
    return <button onClick={this.onClick}>Trace!!</button>
  }
}

export default ConsoleTraceButton;

