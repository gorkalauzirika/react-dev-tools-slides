import React from 'react';

class ConsoleLogButton extends React.Component {
  onClick() {
    console.log('Log!!');
  }

  render() {
    return <button onClick={this.onClick}>Log!!</button>
  }
}

export default ConsoleLogButton;

