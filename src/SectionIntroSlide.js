import React from 'react';
import {Slide, Heading} from 'spectacle';

const SectionIntroSlide = (props) => (
  <Slide transition={["slide"]}  bgImage={props.image} bgDarken={0.75}>
    <Heading size={1} caps fit textColor="primary">
      {props.title}
    </Heading>
  </Slide>
);

export default SectionIntroSlide;
